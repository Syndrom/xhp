<?php
function RecursiveDirectory($dir,&$files,&$dirs,&$dots){
	$iterator = new DirectoryIterator($dir);
	while($iterator->valid()) {
    $file = $iterator->current();
	if($file->isDot())
		$dots++;
	elseif($file->isDir()){
		$dirs[] = $dir.'/'.$file->getFilename();
		RecursiveDirectory($dir.'/'.$file->getFilename(),$files,$dirs,$dots);
	}
	else
		$files[] = $dir.'/'.$file->getFilename();
    $iterator->next();
	}
}
if(!file_exists("xhpize.exe")) die('Can not find xhpize.exe');
//if(!file_exists("stlport_vc10_x.5.2.dll")) die('Can not find stlport_vc10_x.5.2.dll');
$source = ($_GET['src']) ? $_GET['src'] : 'care';
$dest = ($_GET['dest']) ? $_GET['dest'] : 'xhpized';
//check if dest exists. 
if(!is_dir($dest)){
	mkdir($dest,777,true);
}
RecursiveDirectory($source,$files,$dirs,$dots);
foreach($dirs as $dir){
	echo 'Creating '.$dir.'<br />';
	mkdir($dest.'/'.$dir,777,true);
}
foreach($files as $file){
	//copy and xhpize
	echo 'Copying '.$file.'<br />';
	copy($file,$dest.'/'.$file);
	echo exec("xhpize.exe -i ".$dest.'/'.$file);
}
echo ' and '.$dots.' Dots!';
